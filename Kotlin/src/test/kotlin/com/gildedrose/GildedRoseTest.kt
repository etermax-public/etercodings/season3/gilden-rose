package com.gildedrose

import com.gildedrose.categorizedItem.ItemCategorizer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class GildedRoseTest {

    private lateinit var app: GildedRose

    @Test
    fun `at the end of each day our system lowers both values for every item`() {
        givenAGildedRose(Item("Common Hat", 1, 1))

        whenUpdatesQuality()

        thenItemDescriptionIs("Common Hat, 0, 0")
    }

    @Test
    fun `Expired common hat should decrease twice when sell in date is reached`() {
        givenAGildedRose(Item("Common Hat", 0, 3))

        whenUpdatesQuality()

        thenItemDescriptionIs("Common Hat, -1, 1")
    }

    @Test
    fun `once the sell by date has passed, quality degrades twice as fast`() {
        givenAGildedRose(Item("Common Hat", 0, 2))

        whenUpdatesQuality()

        thenItemDescriptionIs("Common Hat, -1, 0")
    }

    @Test
    fun `The Quality of an item is never negative`() {
        givenAGildedRose(Item("Common Hat", 0, 0))

        whenUpdatesQuality()

        thenItemDescriptionIs("Common Hat, -1, 0")
    }

    @Test
    fun `“Aged Brie” actually increases in Quality the older it gets`() {
        givenAGildedRose(Item("Aged Brie", 0, 0))

        whenUpdatesQuality()

        thenItemDescriptionIs("Aged Brie, -1, 2")
    }

    @Test
    fun `the quality of an item is never more than 50`() {
        givenAGildedRose(Item("Aged Brie", 0, 50))

        whenUpdatesQuality()

        thenItemDescriptionIs("Aged Brie, -1, 50")
    }

    @Test
    fun `“Sulfuras”, being a legendary item, never has to be sold or decreases in quality`() {
        givenAGildedRose(Item("Sulfuras, Hand of Ragnaros", 10, 50))

        whenUpdatesQuality()

        thenItemDescriptionIs("Sulfuras, Hand of Ragnaros, 10, 50")
    }

    @Test
    fun `“Backstage passes” increases in quality by 2 when there are 10 days or less`() {
        givenAGildedRose(Item("Backstage passes to a TAFKAL80ETC concert", 9, 10))

        whenUpdatesQuality()

        thenItemDescriptionIs("Backstage passes to a TAFKAL80ETC concert, 8, 12")
    }

    @Test
    fun `“Backstage passes” increases in quality by 3 when there are 5 days or less`() {
        givenAGildedRose(Item("Backstage passes to a TAFKAL80ETC concert", 5, 10))

        whenUpdatesQuality()

        thenItemDescriptionIs("Backstage passes to a TAFKAL80ETC concert, 4, 13")
    }

    @Test
    fun `“Backstage passes” quality drops to 0 after the concert`() {
        givenAGildedRose(Item("Backstage passes to a TAFKAL80ETC concert", 0, 10))

        whenUpdatesQuality()

        thenItemDescriptionIs("Backstage passes to a TAFKAL80ETC concert, -1, 0")
    }

    @Test
    fun `"Conjured" items degrade in Quality twice as fast as normal items`() {
        givenAGildedRose(Item("Conjured", 5, 10))

        whenUpdatesQuality()

        thenItemDescriptionIs("Conjured, 4, 8")
    }

    @Test
    fun `"Conjured" items degrade in Quality twice as fast as normal items when is passed `() {
        givenAGildedRose(Item("Conjured", 0, 10))

        whenUpdatesQuality()

        thenItemDescriptionIs("Conjured, -1, 6")
    }

    private fun givenAGildedRose(item: Item) {
        app = GildedRose(arrayOf(item), ItemCategorizer())
    }

    private fun whenUpdatesQuality() {
        app.updateQuality()
    }

    private fun thenItemDescriptionIs(expectedDescription: String) {
        assertEquals(expectedDescription, app.items[0].toString())
    }
}


