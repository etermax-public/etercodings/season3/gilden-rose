package com.gildedrose

import com.gildedrose.categorizedItem.ItemCategorizer

class GildedRose(var items: Array<Item>, private val itemCategorizer: ItemCategorizer) {

    fun updateQuality() {
        items.forEach { item ->
            val categorizedItem = itemCategorizer.create(item)
            categorizedItem.updateQuality()
        }
    }

}

