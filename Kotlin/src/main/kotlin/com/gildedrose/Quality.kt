package com.gildedrose

class Quality(private val item: Item) {
    fun increase() {
        if (item.quality < 50) {
            item.quality = item.quality + 1
        }
    }
}
