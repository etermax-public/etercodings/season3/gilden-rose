package com.gildedrose

class SellIn(private val item: Item){
    fun decrease() {
        item.sellIn = item.sellIn - 1
    }

    fun isPassed(): Boolean {
        return item.sellIn < 0
    }
}
