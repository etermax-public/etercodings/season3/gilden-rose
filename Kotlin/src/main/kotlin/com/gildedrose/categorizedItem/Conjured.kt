package com.gildedrose.categorizedItem

import com.gildedrose.Item

class Conjured(private val item: Item) : CategorizedItem {
    override fun updateQuality() {
        decreaseQuality(item)
        decreaseQuality(item)

        decreaseSellIn(item)

        if (isPassed(item)) {
            decreaseQuality(item)
            decreaseQuality(item)
        }
    }

    private fun isPassed(item: Item) = item.sellIn < 0

    private fun decreaseSellIn(item: Item) {
        item.sellIn = item.sellIn - 1
    }

    private fun decreaseQuality(item: Item) {
        if (item.quality > 0) {
            item.quality = item.quality - 1
        }
    }

}
