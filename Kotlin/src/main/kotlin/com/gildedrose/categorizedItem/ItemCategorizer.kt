package com.gildedrose.categorizedItem

import com.gildedrose.Item
import com.gildedrose.Quality
import com.gildedrose.SellIn

class ItemCategorizer {

    fun create(item: Item): CategorizedItem {
        return when {
            isAgedBrie(item) -> {
                AgedBrie(SellIn(item), Quality(item))
            }
            isBackstagePasses(item) -> {
                BackstagePasses(item)
            }
            isSulfuras(item) -> {
                Sulfuras()
            }
            isConjured(item) -> {
                Conjured(item)
            }
            else -> {
                CommonItem(item)
            }
        }
    }

    companion object {

        private fun isConjured(item: Item) = item.name == "Conjured"

        private fun isSulfuras(item: Item) = item.name == "Sulfuras, Hand of Ragnaros"

        private fun isBackstagePasses(item: Item) = item.name == "Backstage passes to a TAFKAL80ETC concert"

        private fun isAgedBrie(item: Item) = item.name == "Aged Brie"
    }


}
