package com.gildedrose.categorizedItem

interface CategorizedItem {
    fun updateQuality()
}
