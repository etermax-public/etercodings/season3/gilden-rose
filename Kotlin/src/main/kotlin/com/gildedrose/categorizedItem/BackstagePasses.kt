package com.gildedrose.categorizedItem

import com.gildedrose.Item

class BackstagePasses(private val item: Item) : CategorizedItem {
    override fun updateQuality() {

        item.quality = item.quality + 1

        if (item.sellIn < 11) {
            increaseQuality(item)
        }

        if (item.sellIn < 6) {
            increaseQuality(item)
        }


        decreaseSellIn(item)

        if (isPassed(item)) {
            item.quality = item.quality - item.quality
        }
    }

    private fun increaseQuality(item: Item) {
        if (item.quality < 50) {
            item.quality = item.quality + 1
        }
    }

    private fun isPassed(item: Item) = item.sellIn < 0

    private fun decreaseSellIn(item: Item) {
        item.sellIn = item.sellIn - 1
    }
}
