package com.gildedrose.categorizedItem

import com.gildedrose.Quality
import com.gildedrose.SellIn

class AgedBrie(private val sellIn: SellIn, private val quality: Quality) : CategorizedItem {

    override fun updateQuality() {
        quality.increase()

        sellIn.decrease()

        if (sellIn.isPassed()) {
            quality.increase()
        }
    }
}
